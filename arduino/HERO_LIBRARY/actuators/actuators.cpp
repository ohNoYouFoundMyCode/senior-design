#include "actuators.h"

actuators::actuators(const uint8_t theHighPressurePumpPin, const uint8_t theLowPressurePumpPin, const uint8_t theNormalIntakePin,
	const uint8_t theFlushingIntakePin, const uint8_t theNormalOutputPin, const uint8_t theFlushingOutputPin) : 
	highPressurePumpPin(theHighPressurePumpPin), lowPressurePumpPin(theLowPressurePumpPin), normalIntakePin(theNormalIntakePin),
	flushingIntakePin(theFlushingIntakePin), normalOutputPin(theNormalOutputPin), flushingOutputPin(theFlushingOutputPin)
{
	pinMode(highPressurePumpPin, OUTPUT);
	pinMode(lowPressurePumpPin, OUTPUT);
	pinMode(normalIntakePin, OUTPUT);
	pinMode(flushingIntakePin, OUTPUT);
	pinMode(normalOutputPin, OUTPUT);
	pinMode(flushingOutputPin, OUTPUT);
}

void actuators::killMotor(const uint8_t motor)
{
	digitalWrite(motor, LOW);
}

void actuators::enableMotor(const uint8_t motor)
{
	digitalWrite(motor, HIGH);
}

void actuators::killAllMotors()
{
	digitalWrite(highPressurePumpPin, LOW);
	digitalWrite(lowPressurePumpPin, LOW);
}

void actuators::closeValve(const uint8_t valve)
{
	digitalWrite(valve, LOW);

}

void actuators::openValve(const uint8_t valve)
{
	digitalWrite(valve, HIGH);
}

//Solenoid Valves are all normally closed
void actuators::runMode()
{
	digitalWrite(normalIntakePin, HIGH);
	digitalWrite(flushingIntakePin, LOW);
	digitalWrite(normalOutputPin, HIGH);
	digitalWrite(flushingOutputPin, LOW);
}

void actuators::flushMode()
{
	digitalWrite(normalIntakePin, LOW);
	digitalWrite(flushingIntakePin, HIGH);
	digitalWrite(normalOutputPin, LOW);
	digitalWrite(flushingOutputPin, HIGH);
}

void actuators::allOff()
{
	digitalWrite(highPressurePumpPin, LOW);
	digitalWrite(lowPressurePumpPin, LOW);
	digitalWrite(normalIntakePin, LOW);
	digitalWrite(flushingIntakePin, LOW);
	digitalWrite(normalOutputPin, LOW);
	digitalWrite(flushingOutputPin, LOW);
}