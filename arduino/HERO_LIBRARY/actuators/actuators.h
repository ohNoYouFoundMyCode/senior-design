#ifndef ACTUATORS_H
#define ACTUATORS_H

#include "includesHERO.h"

class actuators
{
public:
	actuators(const uint8_t highPressurePumpPin, const uint8_t lowPressurePumpPin, const uint8_t theNormalIntakePin,
		const uint8_t theFlushingIntakePin, const uint8_t theNormalOutputPin, const uint8_t theFlushingOutputPin);
	void killMotor(const uint8_t motor);
	void enableMotor(const uint8_t motor);
	void killAllMotors();
	void closeValve(const uint8_t valve);
	void openValve(const uint8_t valve);
	void runMode();
	void flushMode();
	void allOff();

private:
	const uint8_t highPressurePumpPin;
	const uint8_t lowPressurePumpPin;
	const uint8_t normalIntakePin;
	const uint8_t flushingIntakePin;
	const uint8_t normalOutputPin;
	const uint8_t flushingOutputPin;
};

#endif
