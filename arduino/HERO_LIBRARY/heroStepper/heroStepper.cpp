#include <Stepper.h>
#include <Arduino.h>
#include "heroStepper.h"

heroStepper::heroStepper(const uint16_t theTotalSteps, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3, const uint8_t pin4, const float theMarginOfError, THEMODE theMode) : 
	Stepper(theTotalSteps, pin1, pin2, pin3, pin4), 
	currentStep(0), totalSteps(theTotalSteps), marginOfError(theMarginOfError), mode(theMode)
{
  // set the speed at 60 rpm:
	setSpeed(60);
	this->stepperTimer = millis();
}

void heroStepper::fullOpen()
{
	for(int i=0; i<200; i++)
	{
		this->step(1);
		delay(10);
	}
	currentStep = 0;
	previousStep = 0;
}

uint8_t heroStepper::convertPSIToStep(uint16_t PSI)
{
	//needs to be updated
	return floor(PSI*1/1);
}

void heroStepper::controller(float desiredPSI, float currentPSI)
{
	if(mode==DEEPDEBUG)
	{
		Serial.print("desiredPSI: ");	Serial.println(desiredPSI, 5);
		Serial.print("currentPSI: ");	Serial.println(currentPSI, 5);
		psiError = desiredPSI-currentPSI;
		Serial.print("psiError: ");	Serial.println(psiError, 5);
	}

	psiError = desiredPSI-currentPSI;
	if(millis()-this->stepperTimer>100)
	{
		if(psiError>marginOfError||psiError<-marginOfError)
		{
			if(psiError>0)
			{
				heroStep(-1);
			}
			else
			{
				heroStep(1);
			}
		}
		this->stepperTimer = millis();
	}
}

float heroStepper::getError()
{
	return psiError;
}

void heroStepper::heroStep(int16_t stepSize)
{
	//previousStep = currentStep;
	//currentStep = currentStep+moveBySteps;

	currentStep -= stepSize;
	//if (currentStep>1500)
//	{
//		currentStep = 1500;
//	}
//	else if (currentStep<0)
//	{
//		currentStep = 0;
//	}
//	else
//	{
		step(stepSize);
//	}

	if(mode==DEEPDEBUG)
	{
		if(stepSize>0)	
		{	
			Serial.print("Opening by ");
			Serial.print(stepSize);
			Serial.println(" steps.");
		}
		else if (stepSize<0)
		{
			Serial.print("Closing by ");
			Serial.print(stepSize*-1);
			Serial.println(" steps.");
		}
		Serial.print("Current Step is ");
		Serial.println(currentStep);
	}
}

int16_t heroStepper::getCurrentStep()
{
	return this->currentStep;
}

int16_t heroStepper::getPreviousStep()
{
	return this->previousStep;
}