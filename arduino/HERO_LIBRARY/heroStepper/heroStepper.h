#ifndef HERO_STEPPER_H
#define HERO_STEPPER_H

#include <Stepper.h>
#include <Arduino.h>
#include "includesHERO.h"

class heroStepper : public Stepper
{
  public:
    //input parameters are:
    heroStepper(const uint16_t theTotalSteps, const uint8_t pin1, const uint8_t pin2, const uint8_t pin3, const uint8_t pin4, const float theMarginOfError, THEMODE theMode);
    void fullOpen();
    uint8_t convertPSIToStep(uint16_t PSI);
    void controller(float desiredPSI, float currentPSI);
    float getError();
    void heroStep(int16_t stepSize);
    int16_t getCurrentStep();
    int16_t getPreviousStep();
    //time reponse function
    //speed controller?

  private:
    const uint16_t totalSteps;
    int16_t currentStep;
    int16_t previousStep;
    int16_t moveBySteps;
    const float marginOfError;
    float psiError;
    int16_t stepSize;
    THEMODE mode;
    float stepperTimer;
};

#endif
