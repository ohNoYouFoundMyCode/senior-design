#ifndef CURRENT_SENSE_H
#define CURRENT_SENSE_H

#include "includesHERO.h"

class currentSensor : public movingAverage
{
public:
    //input parameters are:
    //thePin - the digital pin the sensor is attached to
    //theUpdateRate - desired sample time and printout speed in MILLISECONDS
    //theMode - enum type with choices DEBUG and RUN
    //      --DEBUG will print out to serial port
    //      --RUN will print out to the Xbee RF modem
    currentSensor(const String theName, const uint8_t thePin, const uint16_t theCurrentUpdateRate, THEMODE theMode, const uint16_t theBufferSize);
    uint8_t getCurrentPin();
    void setCurrent();
    float getCurrent();
    void printSerial();
    void printXBee();
    void printError();


private:
  const String name;
  const uint8_t pin;
  const uint16_t currentUpdateRate;
  THEMODE mode;
  float current;
  uint32_t currentTimer;
};
#endif
