#include "currentSensor.h"


currentSensor::currentSensor(const String theName, const uint8_t thePin, const uint16_t theCurrentUpdateRate, THEMODE theMode, const uint16_t theBufferSize) :
movingAverage(theBufferSize),
name(theName), pin(thePin), currentUpdateRate(theCurrentUpdateRate), mode(theMode)
{
  pinMode(pin, INPUT);
  this->currentTimer = millis();
}

uint8_t currentSensor::getCurrentPin()
{
  return this->pin;
}

void currentSensor::setCurrent()
{
  current = analogRead(pin);
  
}

float currentSensor::getCurrent()
{
  this->setCurrent();
  if(millis()-this->currentTimer>this->currentUpdateRate)
  {
    if(mode==DEEPDEBUG)
    {
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
      this->printXBee();
    }
    else
    {
      this->printError();
    }
    this->currentTimer = millis();
  }
  return current;
}

void currentSensor::printSerial()
{
  Serial.println(name);
  Serial.print("Raw Current : ");
  Serial.print(this->current);
  Serial.println(" Amps");
  Serial.print("Filtered Current : ");
  Serial.print(this->getFilteredValue());
  Serial.println(" Amps");
}

void currentSensor::printXBee()
{
  Xbee.print(name);
  Xbee.print(":");
  Xbee.print(this->getFilteredValue());
  Xbee.println(";");
}

void currentSensor::printError()
{
  Serial.print("ERROR!!!! INPUT MODE IS GARBAGE!");
}
