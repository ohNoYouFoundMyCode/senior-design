#include "pressureSensor.h"

pressureSensor::pressureSensor(const String theName, const uint8_t thePin, const uint16_t thePressureUpdateRate, THEMODE theMode, const uint16_t theBufferSize, HIGHORLOW sensorHOL) :
movingAverage(theBufferSize),
name(theName), pin(thePin), pressureUpdateRate(thePressureUpdateRate), mode(theMode), highOrLow(sensorHOL)
{
  pinMode(pin, INPUT);
  this->pressureTimer = millis();
}

uint8_t pressureSensor::getPressurePin()
{
  return this->pin;
}

void pressureSensor::setPressure()
{
  adcReading = analogRead(pin);
  //High Pressure Conversion
  if(highOrLow==SENSOR_HIGH)
  {
    //Pressure Sensor Output
    //1-5v
    //0-1000PSI
    //Voltage divided by 510 and 1K ohm resistor
    //0.662251-3.311258v
    //0-1000PSI
    //12bit ADC Full Scale Range
    //0-4095
    //0-3.3v
    //delV = 3.3/4095
    //12bit ADC actual range
    //821-4095
    //0.662251-3.3v
    //0-995.741PSI

    //delV = 3.3/4095
    //3.311258-3.3 = 0.011258v
    //0.011258/delV = 14 steps over full scale 
    //(4095+14)-821 = over full scall adc range = 3288
    //1000/3288 = 0.304136 = PSI/bin
    //0.304136*(4095-821) = 995.741264 = full scale PSI
    pressure = (adcReading-821)*0.304136+80.0;
  }
 
  //Low Pressure Conversion
  else if(highOrLow==SENSOR_LOW)
  { 
    //Pressure Sensor Output
    //0.5-4.5v
    //0-100PSI
    //Voltage divided by 510 and 1K ohm resistor
    //0.331125-2.980132v
    //0-100PSI
    //12bit ADC Full Scale Range
    //0-4095
    //0-3.3v
    //delV = 3.3/4095 = 0.000805861
    //12bit ADC actual range
    //411-3698
    //0.331125-2.980132v
    //0-100PSI

    //delV = 3.3/4095 = 0.000805861
    //3698-411 = full scall adc range = 3287
    //100/3287 = .030413 = PSI/bin
    pressure = (adcReading-411)*0.030413-15.0;
  }
  else
  {
    Serial.println("ERROR IN PRESSURE HIGH OR LOW");
  }
}

float pressureSensor::getPressure()
{
  this->setPressure();
  if(millis()-this->pressureTimer>this->pressureUpdateRate)
  {
    if(mode==DEEPDEBUG)
    {
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
      this->printXBee();
    }
    else
    {
      this->printError();
    }
    this->pressureTimer = millis();
  }
  return pressure;
}

void pressureSensor::printSerial()
{
  Serial.println(name);
  Serial.print("Raw Pressure: ");
  Serial.print(this->pressure);
  Serial.println(" PSI");
  Serial.print("Filtered Pressure: ");
  Serial.print(this->getFilteredValue());
  Serial.println(" PSI");
}

void pressureSensor::printXBee()
{
  Xbee.print(name);
  Xbee.print(":");
  Xbee.print(this->getFilteredValue());
  Xbee.println(";");
}

void pressureSensor::printError()
{
  Serial.print("ERROR!!!! INPUT MODE IS GARBAGE!");
}