#ifndef PRESSURE_SENSOR_H
#define PRESSURE_SENSOR_H

#include "includesHERO.h"

class pressureSensor : public movingAverage
{
  public:
    //input parameters are:
    //thePin - the digital pin the sensor is attached to
    //theUpdateRate - desired sample time and printout speed in MILLISECONDS
    //theMode - enum type with choices DEBUG and RUN
    //      --DEBUG will print out to serial port
    //      --RUN will print out to the Xbee RF modem
    pressureSensor(const String theName, const uint8_t thePin, const uint16_t thePressureUpdateRate, THEMODE theMode, const uint16_t theBufferSize, HIGHORLOW sensorHOL);
    uint8_t getPressurePin();
    void setPressure();
    float getPressure();
    //prints flow rate to serial monitor
    void printSerial();
    void printXBee();
    void printError();

  private:
    const String name;
    const uint8_t pin;
    const uint16_t pressureUpdateRate;
    THEMODE mode;
    uint16_t adcReading;
    float pressure;
    uint32_t pressureTimer;
    HIGHORLOW highOrLow;
};

#endif

