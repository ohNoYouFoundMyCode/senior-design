#ifndef TEMP_SENSOR_H
#define TEMP_SENSOR_H

#include <OneWire.h>
#include <DallasTemperature.h>
#include "includesHERO.h"

class tempSensor : public movingAverage
{
  public:
    tempSensor(const String theName, const uint8_t thePin, const uint16_t theTempUpdateRate, THEMODE theMode, const uint16_t theBufferSize);
    uint8_t getTempPin();
    void setTemp();
    float getTemp();
    void printSerial();
    void printXBee();
    void printError();

  private:
    const String name;
    const uint8_t pin;
    const uint16_t tempUpdateRate;
    THEMODE mode;
    float temp;
    uint32_t tempTimer;
    OneWire oneWireTempSensor;
    DallasTemperature dallasTempSensor;
};

#endif
