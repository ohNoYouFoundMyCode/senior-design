#include "tempSensor.h"

tempSensor::tempSensor(const String theName, const uint8_t thePin, const uint16_t theTempUpdateRate, THEMODE theMode, const uint16_t theBufferSize) :
movingAverage(theBufferSize),
name(theName), pin(thePin), tempUpdateRate(theTempUpdateRate), mode(theMode), oneWireTempSensor(thePin), dallasTempSensor(&oneWireTempSensor)
{
  pinMode(pin, INPUT);
  this->tempTimer = millis();
}

uint8_t tempSensor::getTempPin()
{
  return this->pin;
}

void tempSensor::setTemp()
{

  this->dallasTempSensor.requestTemperatures();
  this->temp=this->dallasTempSensor.getTempFByIndex(0);
  if(millis()-tempTimer>tempUpdateRate)
  {
    if(mode==DEEPDEBUG)
    {
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
     this->printXBee();
    }
    else
    {
      this->printError();
    }
    tempTimer = millis();
  }
  //Serial.print("The temp inside setTemp is: ");
  //Serial.println(temp);
}

float tempSensor::getTemp()
{
  this->setTemp();
  return this->temp;
}

void tempSensor::printSerial()
{
  Serial.println(name);
  Serial.print("Raw Temperature is: ");
  Serial.print(temp);
  Serial.println(" Deg F");
  Serial.print("Filtered Temperature is: ");
  Serial.print(this->getFilteredValue());
  Serial.println(" Deg F");
}

void tempSensor::printXBee()
{
  Xbee.print(name);
  Xbee.print(":");
  Xbee.print(this->getFilteredValue());
  Xbee.println(";");
}

void tempSensor::printError()
{
  Serial.print("ERROR!!!! INPUT MODE IS GARBAGE!");
}
