#ifndef INCLUDES_H
#define INCLUDES_H

enum THEMODE{NOMODE, DEEPDEBUG, SERIALPRINT, XBEEPRINT};
enum HIGHORLOW{SENSOR_HIGH, SENSOR_LOW};
enum RUNMODE{FLUSH, RUN, DONOTHING};

#define Xbee Serial3

#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <SD.h>
#include <Stepper.h>

#include <movingAverage.h>

#endif