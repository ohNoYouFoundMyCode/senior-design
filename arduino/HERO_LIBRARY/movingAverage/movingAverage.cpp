#include "movingAverage.h"

movingAverage::movingAverage(const uint16_t theBufferSize) : bufferSize(theBufferSize)
{
	valueBuffer = new float[bufferSize];
	for(int i=0; i<bufferSize; i++)
	{
		valueBuffer[i] = -65535;
	}
}

movingAverage::~movingAverage()
{
	delete[] valueBuffer;
}

void movingAverage::addValue(float theVal)
{
	for(int i=0; i<bufferSize; i++)
	{
		valueBuffer[bufferSize-1-i]=valueBuffer[bufferSize-1-i-1];
	}
	//Serial.print(theVal);
	//Serial.println(" added");
	valueBuffer[0]=theVal;
}

void movingAverage::printAverage()
{
	Serial.print("the buffer size is:");
	Serial.println(bufferSize);
	for(int i=0; i<bufferSize; i++)
	{
		Serial.print("The buffer val for:");
		Serial.print(i);
		Serial.print(" is:");
		Serial.println(valueBuffer[i]);
	}
	Serial.print("The filteredValue is: ");
	Serial.println(this->getFilteredValue());
}

bool movingAverage::bufferIsFull()
{
	return 	valueBuffer[bufferSize-1]!=-65535;
}

float movingAverage::getFilteredValue()
{
	float temp=0;;
	for(int i=0; i<bufferSize; i++)
	{
		temp+=valueBuffer[i];
	}
	//Serial.println(bufferSize);
	float tempBuf = bufferSize*1.0;
	filteredValue=temp/tempBuf;
	return filteredValue;
}
