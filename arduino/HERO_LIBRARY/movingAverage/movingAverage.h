#ifndef MOVING_AVERAGE_H
#define MOVING_AVERAGE_H

#include "includesHERO.h"

class movingAverage
{
public:
	movingAverage(uint16_t theBufferSize);
	~movingAverage();
	void addValue(float val);
	void printAverage();
	bool bufferIsFull();
	float getFilteredValue();

public:
	const uint16_t bufferSize;
	float *valueBuffer;
	float filteredValue;

};

#endif