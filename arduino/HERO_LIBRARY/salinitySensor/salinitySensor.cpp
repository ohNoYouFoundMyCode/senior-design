#include "salinitySensor.h"

salinitySensor::salinitySensor(const String salName, const uint8_t salPin, const uint16_t theSalUpdateRate, THEMODE theMode, const float theR1, 
	const float theECGround, const float theECPower, const float theConversionFactorPPM,
	const float theTemperatureCoef, const float theK, const uint16_t theBufferSize, const String tempName, const uint8_t tempPin, const uint16_t theTempUpdateRate) :
movingAverage(theBufferSize),
name(salName), pin(salPin), salinityUpdateRate(theSalUpdateRate), mode(theMode),  R1(theR1), ECGround(theECGround),
	ECPower(theECPower), conversionFactorPPM(theConversionFactorPPM), temperatureCoef(theTemperatureCoef), k(theK), 
theTempSensor(tempName, tempPin, theTempUpdateRate, theMode, theBufferSize)
{
  pinMode(pin, INPUT);
  this->salinityTimer = millis();
}

uint8_t salinitySensor::getSalinityPin()
{
  return this->pin;
}

void salinitySensor::setSalinity()
{
  raw = analogRead(pin);
  Vdrop = (ECPower*raw*1.0)/4095.0;
  Rc=(Vdrop*R1)/(ECPower-Vdrop);
  //Rc=Rc-Ra; //acounting for Digital Pin Resitance
  EC = 1000.0/(Rc*k);
  
  //localTemp = theTempSensor.getTemp();
  //EC25  =  EC/(1+temperatureCoef*(localTemp-25.0));
  
  ppm = EC*(conversionFactorPPM*1000.0); //this ppm is not compensate to 25*C
  //ppm25 = EC25*conversionFactorPPM*1000.0;
  
  if(mode==DEEPDEBUG)
  {
    Serial.println(raw);
    Serial.println(Vdrop);
    Serial.println(Rc);
    Serial.println(EC);
    Serial.println(ppm);
  }

  if (name=="Low Salinity")
  {
    //Serial.println("In Low");
    linearCurveFittedPPM = 1.853*ppm;   // linear
    //linearCurveFittedPPM25 = linearCurveFittedPPM/0.924;
  }
  else if (name == "High Salinity")
  {
    //Serial.println("In High");
    linearCurveFittedPPM = 23.8*ppm;
    //Serial.println(linearCurveFittedPPM);
  }
}

float salinitySensor::getSalinity()
{
  this->setSalinity();
  if(millis()-salinityTimer>salinityUpdateRate)
  {
    if(mode==DEEPDEBUG)
    {
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
     this->printXBee();
    }
    else
    {
      this->printError();
    }
    salinityTimer = millis();
  }
  return this->linearCurveFittedPPM;
}

void salinitySensor::printSerial()
{
  Serial.println(name);
  Serial.print("Instant Curve Fit Salinity is: ");
  Serial.print(ppm);
  Serial.print(" PPM, Filtered Curve Fit Salinity is: ");
  Serial.print(this->getFilteredValue());
  Serial.println(" PPM");
}

void salinitySensor::printXBee()
{
  Xbee.print(name);
  Xbee.print(":");
  Xbee.print(this->getFilteredValue());
  Xbee.println(";");
}

void salinitySensor::printError()
{
  Serial.print("ERROR!!!! INPUT MODE IS GARBAGE!");
}
