#ifndef SALINITY_SENSOR_H
#define SALINITY_SENSOR_H

#include <OneWire.h>
#include <DallasTemperature.h>
#include <tempSensor.h>
#include "includesHERO.h"

class salinitySensor : public movingAverage
{
public:
	salinitySensor(const String salName, const uint8_t salPin, const uint16_t theSalUpdateRate, THEMODE theMode, const float theR1, 
	const float theECGround, const float theECPower, const float theConversionFactorPPM,
	const float theTemperatureCoef, const float k, const uint16_t theBufferSize, const String tempName, const uint8_t tempPin, const uint16_t theTempUpdateRate);
    uint8_t getSalinityPin();
    void setSalinity();
    float getSalinity();
    void printSerial();
    void printXBee();
    void printError();
    tempSensor theTempSensor;

private:
    const String name;
	const uint8_t pin;
	const uint16_t salinityUpdateRate;
  	THEMODE mode;
	const float R1;
	const float ECGround;
	const float ECPower;
	const float conversionFactorPPM;
	const float temperatureCoef;
	const float k;

  	uint32_t salinityTimer;

	float localTemp;
	float raw;
	float Vdrop;
	float Rc;
	float EC;
	float EC25;
	float ppm;
	float ppm25;
	float x2, x3, x4, x5, x6;
	float x2_25, x3_25, x4_25, x5_25, x6_25;
	float linearCurveFittedPPM;
	float linearCurveFittedPPM25;
	float exponentialCurveFittedPPM;
	float exponentialCurveFittedPPM25;
};

#endif
