#include <SPI.h>
#include <SD.h>
#include "sdCard.h"

sdCard::sdCard(const uint8_t theChipSelect) : chipSelect(theChipSelect)
{
  pinMode(chipSelect, OUTPUT);
}

void sdCard::setup()
{
  if(!SD.begin(chipSelect))
  {
    Serial.println("Card failed, or not present");
  }
  else
  {
    Serial.println("Card open success");
    theFile = SD.open("data.csv", O_WRITE|O_CREAT);
  }
	if(theFile)
  {
    String dataString = "Time(millis), Low Salinity, High Salinity, Intake Temperature, Product Temperature, Intake Flow, "
    	"Product Flow, Brine Flow, Low Pressure, High Pressure, AC Current, DC Current, PSI Error";
    theFile.println(dataString);
    theFile.close();
  }
  else
  {
  	Serial.println("Error opening theFile for sdCard setup");
  }
}

void sdCard::read()
{

}

void sdCard::write(float theMillis, float lowSalinity, float highSalinity, float theIntakeTemp, float theProductTemp, float intakeFlow, float productFlow,
        float brineFlow, float lowPressure, float highPressure, float acCurrent, float dcCurrent, float psiError)
{
    char dataString[1000];
    sprintf(dataString, "%.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f", theMillis, lowSalinity, highSalinity, theIntakeTemp,
      theProductTemp, intakeFlow, productFlow, brineFlow, lowPressure, highPressure, acCurrent, dcCurrent, psiError);
    theFile = SD.open("data.csv", O_WRITE|O_CREAT);
    theFile.println(dataString);
    theFile.close();
}