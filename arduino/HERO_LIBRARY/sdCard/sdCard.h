#ifndef SD_CARD_H
#define SD_CARD_H

#include <SPI.h>
#include <SD.h>
#include "includesHERO.h"

class sdCard
{
  public:
    //input parameters are:
    sdCard(const uint8_t theChipSelect);
    void setup();
    void read();
    void write(float theMillis, float lowSalinity, float highSalinity, float theIntakeTemp, float theProductTemp, float intakeFlow, float productFlow,
        float brineFlow, float lowPressure, float highPressure, float acCurrent, float dcCurrent, float psiError);

  private:
    const uint8_t chipSelect;
    File theFile;
    String dataString = "";
};

#endif