#include "flowMeter.h"

flowMeter::flowMeter(const String theName, const uint8_t thePin, const uint16_t theFlowUpdateRate, THEMODE theMode, const uint16_t theBufferSize) :
movingAverage(theBufferSize),
name(theName), pin(thePin), flowUpdateRate(theFlowUpdateRate), mode(theMode)
{
  pinMode(pin, INPUT);
  this->flowTimer = millis();
}

uint8_t flowMeter::getFlowPin()
{
  return this->pin;
}

//Equation acording to ADAfruit is: Sensor Frequency (Hz) = 7.5 * Q (Liters/min)
//This means we will measure the number of counts in 1 second, divide by 7.5, 
//and then divide by 3.78541 to convert from L/m to Gallons/m.
//TODO - convert this from counting ticks per second to counting set number of ticks and measuring time
void flowMeter::setFlowRate(volatile uint16_t &theFlowCount)
{
  if(millis()-this->flowTimer>this->flowUpdateRate)
  {
    this->flowRate = this->flowRate*flowUpdateRate/4095.0000;
    this->flowRate = theFlowCount / 7.5000;
    this->flowRate = this->flowRate / 3.7850;
    if(mode==DEEPDEBUG)
    {
      Serial.print("The count is ");
      Serial.println(theFlowCount);
      Serial.print("The flowRate is ");
      Serial.println(this->flowRate);
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
      this->printXBee();
    }
    else
    {
      this->printError();
    }
    this->flowTimer = millis();
    theFlowCount = 0;
  }
}

float flowMeter::getFlowRate(volatile uint16_t &theFlowCount)
{
  this->setFlowRate(theFlowCount);
  //Time set to zero the count every 1 second
  return this->flowRate;
}

float flowMeter::productGetFlowRate(float difference)
{
  flowRate = difference;
  if(millis()-this->flowTimer>this->flowUpdateRate)
  {
    if(mode==DEEPDEBUG)
    {
      Serial.print("The flowRate is ");
      Serial.println(this->flowRate);
    }
    else if (mode==SERIALPRINT)
    {
      this->printSerial();
    }
    else if (mode==XBEEPRINT)
    {
      this->printXBee();
    }
    else
    {
      this->printError();
    }
    this->flowTimer = millis();
  }
  return this->flowRate;
}

void flowMeter::printSerial()
{
  Serial.println(name);
  Serial.print("Raw Flow rate: ");
  Serial.print(this->flowRate);
  Serial.println(" gal/min");
  Serial.print("Filtered Flow rate: ");
  Serial.print(this->getFilteredValue());
  Serial.println(" gal/min");
}

void flowMeter::printXBee()
{
  Xbee.print(name);
  Xbee.print(":");
  Xbee.print(this->getFilteredValue());
  Xbee.println(";");
}

void flowMeter::printError()
{
  Serial.print("ERROR!!!! INPUT MODE IS GARBAGE!");
}
