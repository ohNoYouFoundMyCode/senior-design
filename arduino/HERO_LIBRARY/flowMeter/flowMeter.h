#ifndef FLOW_METER_H
#define FLOW_METER_H

#include "includesHERO.h"

class flowMeter : public movingAverage
{
  public:
    //input parameters are:
    //thePin - the digital pin the sensor is attached to
    //theUpdateRate - desired sample time and printout speed in MILLISECONDS
    //theMode - enum type with choices DEBUG and RUN
    //      --DEBUG will print out to serial port
    //      --RUN will print out to the Xbee RF modem
    flowMeter(const String theName, const uint8_t thePin, const uint16_t theFlowUpdateRate, THEMODE theMode, const uint16_t theBufferSize);
    //returns the flow pin, used for interrup
    uint8_t getFlowPin();
    //converts from number of readings on the interrupt in a second to gallons per minute
    //Equation acording to ADAfruit is: Sensor Frequency (Hz) = 7.5 * Q (Liters/min)
    //This means we will measure the number of counts in 1 second, divide by 7.5, 
    //and then divide by 3.78541 to convert from L/m to Gallons/m.
    void setFlowRate(volatile uint16_t &theFlowCount);
    //returns the flow rate in Gallons per minute
    float getFlowRate(volatile uint16_t &theFlowCount);
    float productGetFlowRate(float difference);
    //prints flow rate to serial monitor
    void printSerial();
    void printXBee();
    void printError();

  private:
    const String name;
    const uint8_t pin;
    const uint16_t flowUpdateRate;
    THEMODE mode;
    float flowRate;
    uint32_t flowTimer;
};

#endif
