#include "includesHERO.h"
#include "tempSensor.h"
#include "salinitySensor.h"
#include "flowMeter.h"
#include "pressureSensor.h"
#include "currentSensor.h"
#include "actuators.h"
#include "sdCard.h"
#include "heroStepper.h"

THEMODE mode = DEBUG;

//salinity and temp sensor objects
const String lowSalName = "Low Salinity";
const String highSalName = "High Salinity";
const uint8_t salinityPinLow = A5;//C_1_DATA_IN
const uint8_t salinityPinHigh = A4;//C_2_DATA_IN
const uint16_t theSalinityUpdateRateLow = 1000.0;
const uint16_t theSalinityUpdateRateHigh = 1000.0;
const uint8_t R1_Low = 1300;
const uint8_t Ra_Low = 700;
const uint8_t R1_High = 1300;
const uint8_t Ra_High = 700;
const uint8_t ECGroundLow = 0.0;
const uint8_t ECPowerLow = 5.0;
const uint8_t ECGroundHigh = 0.0;
const uint8_t ECPowerHigh = 5.0;
const float ConversionFactorPPMLow = 0.64;
const float TemperatureCoefLow = 0.019;
const float ConversionFactorPPMHigh = 0.43;
const float TemperatureCoefHigh = 0.019;
const float kLow = 0.43;
const float kHigh = 0.43;
const uint16_t salinityBufferSize = 20;

const String intakeTempName = "Intake Temperature";
const String productTempName = "Product Temperature";
const uint8_t intakeTempPin = A0;
const uint8_t productTempPin = A1;
const uint16_t lowTempUpdateRate = 1000.0;
const uint16_t highTempUpdateRate = 1000.0;
const uint16_t tempBufferSize = 20; //unused, currently set as same as salinity update rate
  
salinitySensor salinitySensorLow(lowSalName, salinityPinLow, theSalinityUpdateRateLow, mode, R1_Low, Ra_Low, ECGroundLow, ECPowerLow, ConversionFactorPPMLow, 
  TemperatureCoefLow, kLow, salinityBufferSize, intakeTempName, productTempPin, lowTempUpdateRate);
salinitySensor salinitySensorHigh(highSalName, salinityPinHigh, theSalinityUpdateRateHigh, mode, R1_High, Ra_High, ECGroundHigh, ECPowerHigh, ConversionFactorPPMHigh, 
  TemperatureCoefHigh, kHigh,salinityBufferSize, productTempName, intakeTempPin, highTempUpdateRate);

//flow meter objects and methods
const String intakeFlowName = "Intake Flow";
const String productFlowName = "Product Flow";
const String brineFlowName = "Brine Flow";
const uint8_t intakeFlowPin = 51;//F_1_DATA
const uint8_t productFlowPin = 53;//F_2_DATA
const uint8_t brineFlowPin = 49;//F_3_DATA
const uint16_t intakeFlowUpdateRate = 1000.0;
const uint16_t produceFlowUpdateRate = 1000.0;
const uint16_t brineFlowUpdateRate = 1000.0;
volatile uint16_t intakeFlowCount;
volatile uint16_t productFlowCount;
volatile uint16_t brineFlowCount;
const uint16_t intakeFlowBufferSize = 20;
const uint16_t productFlowBufferSize = 20;
const uint16_t brineFlowBufferSize = 20;
flowMeter intakeFlowMeter(intakeFlowName, intakeFlowPin, intakeFlowUpdateRate, mode, intakeFlowBufferSize);
flowMeter productFlowMeter(productFlowName, productFlowPin, produceFlowUpdateRate, mode, productFlowBufferSize);
flowMeter brineFlowMeter(brineFlowName, brineFlowPin, brineFlowUpdateRate, mode, brineFlowBufferSize);
void intakeISR();
void productISR();
void brineISR();

//pressure sensor objects
const String lowPressureName = "Low Pressure";
const String highPressureName= "High Pressure";
const uint8_t pressurePinLow = A2;//P_1_DATA_IN
const uint8_t pressurePinHigh = A7;//P_2_DATA_IN
const uint16_t thePressureUpdateRateLow = 1000.0;
const uint16_t thePressureUpdateRateHigh = 1000.0;
const uint16_t pressureBufferSize = 20;
HIGHORLOW lowSal = SENSOR_LOW;
HIGHORLOW highSal = SENSOR_HIGH;
pressureSensor pressureSensorLow(lowPressureName, pressurePinLow, thePressureUpdateRateLow, mode, pressureBufferSize, lowSal);
pressureSensor pressureSensorHigh(highPressureName, pressurePinHigh, thePressureUpdateRateHigh, mode,pressureBufferSize, highSal);

//current sensor objects
const String acCurrentName = "AC Current";
const String dcCurrentName = "DC Current";
const uint8_t acCurrentPin = A0;//AC_CURRENT_SENSOR_OUT
const uint8_t dcCurrentPin = A11;//DC_CURRENT_SENSOR_OUT
const uint16_t currentUpdateRate = 1000.0;
const uint16_t currentBufferSize = 20;
currentSensor acCurrentSensor(acCurrentName, acCurrentPin, currentUpdateRate, mode, currentBufferSize);
currentSensor dcCurrentSensor(dcCurrentName, dcCurrentPin, currentUpdateRate, mode, currentBufferSize);

//actuator objects
const uint8_t highPressureMotorPin = 23;//HPP_RELAY
const uint8_t lowPressureMotorPin = 22;//LPP_RELAY
//All SV valves are normally closed
//during normal operation normal should be written high
//during flushing operation flushing should be written high
const uint8_t normalIntakeSV = 30;//SOLENOID_VALVE_1
const uint8_t flushingIntakeSV = 31;//SOLENOID_VALVE_2
const uint8_t normalOutputSV = 28;//SOLENOID_VALVE_3
const uint8_t flushingOutputSV = 25;//SOLENOID_VALVE_4
actuators theActuators(highPressureMotorPin, lowPressureMotorPin, normalIntakeSV, flushingIntakeSV, normalOutputSV, flushingOutputSV);

//SD Card objects
const uint8_t theChipSelect = 32;//SD_CS
sdCard theSDCard(theChipSelect);
uint32_t SDTimer;
const uint16_t SDUpdateRate = 1000.0;

//stepper objects
const uint8_t totalNumberOfSteps = 200;
const uint8_t pin1 = 10;//STEPPER_1_BLUE_22AWG
const uint8_t pin2 = 11;//STEPPER_2_BLUE_22AWG
const uint8_t pin3 = 12;//STEPPER_3_BLUE_22AWG
const uint8_t pin4 = 13;//STEPPER_4_BLUE_22AWG
const float theMarginOfError = 5.0;
heroStepper theStepper(totalNumberOfSteps, pin1, pin2, pin3, pin4, theMarginOfError);

void setup()
{
  analogReadResolution(12);
  Serial.begin(9600);
  //flow meter interrupts
  attachInterrupt(intakeFlowMeter.getFlowPin(), intakeISR, RISING);
  attachInterrupt(productFlowMeter.getFlowPin(), productISR, RISING);
  attachInterrupt(brineFlowMeter.getFlowPin(), brineISR, RISING);
  
  //Flow
  while(!intakeFlowMeter.bufferIsFull())
  {
    intakeFlowMeter.addValue(intakeFlowMeter.setFlowRate(intakeFlowCount));    
  }
  while(!productFlowMeter.bufferIsFull())
  {
    productFlowMeter.addValue(productFlowMeter.setFlowRate(productFlowCount));    
  }
  while(!brineFlowMeter.bufferIsFull())
  {
    brineFlowMeter.addValue(brineFlowMeter.setFlowRate(brineFlowCount));    
  }
  Serial.println("Entering the Loop");
}

void loop()
{
  Serial.println("Inside the Loop");
  intakeFlowMeter.addValue(intakeFlowMeter.setFlowRate(intakeFlowCount));    
  productFlowMeter.addValue(productFlowMeter.setFlowRate(productFlowCount));    
  brineFlowMeter.addValue(brineFlowMeter.setFlowRate(brineFlowCount));
}//end of loop

void intakeISR()
{
  intakeFlowCount++;
}

void productISR()
{
  productFlowCount++;
}

void brineISR()
{
  brineFlowCount++;
}
