#include "includesHERO.h"
#include "tempSensor.h"
#include "salinitySensor.h"
#include "flowMeter.h"
#include "pressureSensor.h"
#include "currentSensor.h"
#include "actuators.h"
#include "sdCard.h"
#include "heroStepper.h"

THEMODE mode = XBEEPRINT;
RUNMODE runMode = DONOTHING;
float desiredPSI;

#define Xbee Serial3

//salinity and temp sensor objects
const String lowSalName = "Low Salinity";
const String highSalName = "High Salinity";
const uint8_t salinityPinLow = 4;//C_2_DATA_IN
const uint8_t salinityPinHigh = 5;//C_1_DATA_IN
const uint16_t theSalinityUpdateRateLow = 1000.0;
const uint16_t theSalinityUpdateRateHigh = 1000.0;
const float R1_Low = 390;
const float R1_High = 300;
const float ECGroundLow = 0.0;
const float ECPowerLow = 3.3;
const float ECGroundHigh = 0.0;
const float ECPowerHigh = 3.3;
const float ConversionFactorPPMLow = 0.64;
const float TemperatureCoefLow = 0.019;
const float ConversionFactorPPMHigh = 0.64;
const float TemperatureCoefHigh = 0.019;
const float kLow = 0.58;
const float kHigh = 0.58;
const uint16_t salinityBufferSize = 50;

const String intakeTempName = "Intake Temperature";
const String productTempName = "Product Temperature";
//const uint8_t intakeTempPin = 50;
const uint8_t productTempPin = 52;
const uint16_t lowTempUpdateRate = 1000.0;
const uint16_t highTempUpdateRate = 1000.0;
const uint16_t tempBufferSize = 100; //unused, currently set as same as salinity update rate
  
salinitySensor salinitySensorLow(lowSalName, salinityPinLow, theSalinityUpdateRateLow, mode, R1_Low, ECGroundLow, ECPowerLow, ConversionFactorPPMLow, 
  TemperatureCoefLow, kLow, salinityBufferSize, productTempName, productTempPin, highTempUpdateRate);
salinitySensor salinitySensorHigh(highSalName, salinityPinHigh, theSalinityUpdateRateHigh, mode, R1_High, ECGroundHigh, ECPowerHigh, ConversionFactorPPMHigh, 
  TemperatureCoefHigh, kHigh,salinityBufferSize, intakeTempName, productTempPin, highTempUpdateRate);

//flow meter objects and methods
const String intakeFlowName = "Intake Flow";
const String productFlowName = "Product Flow";
const String brineFlowName = "Brine Flow";
const uint8_t intakeFlowPin = 51;//F_1_DATA
const uint8_t productFlowPin = 53;//F_2_DATA
const uint8_t brineFlowPin = 49;//F_3_DATA
const uint16_t theFlowUpdateRate = 1000.0;
volatile uint16_t intakeFlowCount;
volatile uint16_t productFlowCount;
volatile uint16_t brineFlowCount;
const uint16_t flowBufferSize = 50;
flowMeter intakeFlowMeter(intakeFlowName, intakeFlowPin, theFlowUpdateRate, mode, flowBufferSize);
flowMeter productFlowMeter(productFlowName, productFlowPin, theFlowUpdateRate, mode, flowBufferSize);
flowMeter brineFlowMeter(brineFlowName, brineFlowPin, theFlowUpdateRate, mode, flowBufferSize);
void intakeISR();
void productISR();
void brineISR();

//pressure sensor objects
const String lowPressureName = "Low Pressure";
const String highPressureName= "High Pressure";
const uint8_t pressurePinLow = A2;//P_1_DATA_IN
const uint8_t pressurePinHigh = A7;//P_2_DATA_IN
const uint16_t thePressureUpdateRateLow = 1000.0;
const uint16_t thePressureUpdateRateHigh = 1000.0;
const uint16_t pressureBufferSize = 1;
HIGHORLOW lowSal = SENSOR_LOW;
HIGHORLOW highSal = SENSOR_HIGH;
pressureSensor pressureSensorLow(lowPressureName, pressurePinLow, thePressureUpdateRateLow, mode, pressureBufferSize, lowSal);
pressureSensor pressureSensorHigh(highPressureName, pressurePinHigh, thePressureUpdateRateHigh, mode, pressureBufferSize, highSal);

//current sensor objects
const String acCurrentName = "AC Current";
const String dcCurrentName = "DC Current";
const uint8_t acCurrentPin = A0;//AC_CURRENT_SENSOR_OUT
const uint8_t dcCurrentPin = A11;//DC_CURRENT_SENSOR_OUT
const uint16_t currentUpdateRate = 1000.0;
const uint16_t currentBufferSize = 50;
currentSensor acCurrentSensor(acCurrentName, acCurrentPin, currentUpdateRate, mode, currentBufferSize);
currentSensor dcCurrentSensor(dcCurrentName, dcCurrentPin, currentUpdateRate, mode, currentBufferSize);

//actuator objects
const uint8_t highPressureMotorPin = 22;//HPP_RELAY
const uint8_t lowPressureMotorPin = 23;//LPP_RELAY
//All SV valves are normally closed
//during normal operation normal should be written high
//during flushing operation flushing should be written high
const uint8_t normalIntakeSV = 30;//SOLENOID_VALVE_1
const uint8_t flushingIntakeSV = 31;//SOLENOID_VALVE_2
const uint8_t normalOutputSV = 28;//SOLENOID_VALVE_3
const uint8_t flushingOutputSV = 25;//SOLENOID_VALVE_4
actuators theActuators(highPressureMotorPin, lowPressureMotorPin, normalIntakeSV, flushingIntakeSV, normalOutputSV, flushingOutputSV);

//SD Card objects
const uint8_t theChipSelect = 32;//SD_CS
sdCard theSDCard(theChipSelect);
uint32_t SDTimer;
const uint16_t SDUpdateRate = 1000.0;

//stepper objects
const uint8_t stepsPerRevolution = 200;
const uint8_t pin1 = 10;//STEPPER_1_BLUE_22AWG
const uint8_t pin2 = 12;//STEPPER_2_BLUE_22AWG
const uint8_t pin3 = 8;//STEPPER_3_BLUE_22AWG
const uint8_t pin4 = 13;//STEPPER_4_BLUE_22AWG
const float theMarginOfError = 20.0;
heroStepper theHeroStepper(stepsPerRevolution, pin1, pin2, pin3, pin4, theMarginOfError, mode);

uint32_t systemRunTime;
uint32_t systemFlushTime;

void setup()
{
  analogReadResolution(12);
  Serial.begin(115200);
  Xbee.begin(9600);
  //flow meter interrupts
  attachInterrupt(intakeFlowMeter.getFlowPin(), intakeISR, RISING);
  attachInterrupt(brineFlowMeter.getFlowPin(), brineISR, RISING);
  //We use analog pin 2 to pwm the enable pins of the stepper motor controller
  //the value of 55 was chosen experimentally
  analogWrite(2, 255);

  theSDCard.setup();
  SDTimer = millis();

  //Salinity
  while(!salinitySensorLow.bufferIsFull())
  {
    salinitySensorLow.addValue(salinitySensorLow.getSalinity());    
  }
  while(!salinitySensorHigh.bufferIsFull())
  {
    salinitySensorHigh.addValue(salinitySensorHigh.getSalinity());    
  }
  
  //Temperature
  //while(!salinitySensorLow.theTempSensor.bufferIsFull())
  //{
  //  salinitySensorLow.theTempSensor.addValue(salinitySensorLow.theTempSensor.getTemp()); 
  //}
  //while(!salinitySensorHigh.theTempSensor.bufferIsFull())
  //{
  //  salinitySensorHigh.theTempSensor.addValue(salinitySensorHigh.theTempSensor.getTemp());  
  //}
  
  //Flow
  while(!intakeFlowMeter.bufferIsFull())
  {
    intakeFlowMeter.addValue(intakeFlowMeter.getFlowRate(intakeFlowCount));    
  }
  while(!productFlowMeter.bufferIsFull())
  {
    productFlowMeter.addValue(intakeFlowMeter.getFlowRate(intakeFlowCount)-brineFlowMeter.getFlowRate(brineFlowCount));    
  }
  while(!brineFlowMeter.bufferIsFull())
  {
    brineFlowMeter.addValue(brineFlowMeter.getFlowRate(brineFlowCount));    
  }
  
  //Pressure
  while(!pressureSensorLow.bufferIsFull())
  {
    pressureSensorLow.addValue(pressureSensorLow.getPressure());    
  }
  while(!pressureSensorHigh.bufferIsFull())
  {
    pressureSensorHigh.addValue(pressureSensorHigh.getPressure());    
  }
  
  //Current
  while(!acCurrentSensor.bufferIsFull())
  {
    acCurrentSensor.addValue(acCurrentSensor.getCurrent());    
  }
  while(!dcCurrentSensor.bufferIsFull())
  {
    dcCurrentSensor.addValue(dcCurrentSensor.getCurrent());    
  }

  mode=XBEEPRINT;
  theHeroStepper.fullOpen();
  if(mode==SERIALPRINT)
  {
    bool theFlag = false;
    char key;
    Serial.println("Options: Run System (R), Flush System (F), Open Needle Vavle (-), Close Needle Valve (+)");
    while(!theFlag)
    {
      key = (char)Serial.read();
      if(key=='R')
      {
        Serial.println("Solenoid Valves in Flush Mode");
        theActuators.runMode();
        Serial.println("Low pressure pump enabled");
        theActuators.enableMotor(lowPressureMotorPin);
        delay(2500);
        Serial.println("High pressure pump enabled");
        theActuators.enableMotor(highPressureMotorPin);
        Serial.println("How long would you like the system to run for?");
        while(!Serial.available()>0)
        {
        }
        systemRunTime = Serial.parseInt();
        systemFlushTime = 1000*60*10;
        theFlag = true;
        runMode = RUN;
      }
      else if(key=='F')
      {
        Serial.println("Solenoid Valves in Flush Mode");
        theActuators.flushMode();
        Serial.println("Low pressure pump enabled");
        theActuators.enableMotor(lowPressureMotorPin);
        delay(2500);
        Serial.println("High pressure pump enabled");
        theActuators.enableMotor(highPressureMotorPin);
        Serial.println("How long would you like the system to run for?");
        while(!Serial.available()>0)
        {
        }
        systemFlushTime = Serial.parseInt();
        theFlag = true;
        runMode = FLUSH;
      }
      else if(key=='-')
      {
        Serial.println("Manual Open");
        theHeroStepper.heroStep(10);
      }
      else if(key=='+')
      {
        Serial.println("Manual Close");
        theHeroStepper.heroStep(-10);
      }
    }
  }
  else if(mode==XBEEPRINT)
  {
    bool theFlag = false;
    Xbee.println("$Options: Run System (R), Flush System (F), Open Needle Valve (-), Close Needle Valve (+)$");
    while(!theFlag)
    {
      char key;
      key = Xbee.read();
      if(key=='R')
      {
        Xbee.println("Solenoid Valves in Run Mode");
        theActuators.runMode();
        Xbee.println("Low pressure pump enabled");
        theActuators.enableMotor(lowPressureMotorPin);
        delay(2500);
        Xbee.println("High pressure pump enabled");
        theActuators.enableMotor(highPressureMotorPin);
        while(!Xbee.available()>0)
        {
        }
        systemRunTime = Xbee.parseInt();
        Xbee.print("System will run for:");
        Xbee.println(systemRunTime);
        
        while(!Xbee.available()>0)
        {
        }
        systemFlushTime = Xbee.parseInt();
        Xbee.print("System will flush for:");
        Xbee.println(systemFlushTime);
        
        while(!Xbee.available()>0)
        {
        }
        desiredPSI = Xbee.parseFloat();
        Xbee.print("System pressure set to:");
        Xbee.println(desiredPSI);
        theFlag = true;
        runMode = RUN;
      }
      else if(key=='F')
      {
        Xbee.println("Solenoid Valves in Flush Mode");
        theActuators.flushMode();
        Xbee.println("Low pressure pump enabled");
        theActuators.enableMotor(lowPressureMotorPin);
        delay(2500);
        Xbee.println("High pressure pump enabled");
        theActuators.enableMotor(highPressureMotorPin);
        Xbee.println("How long would you like the system to run for?");
        systemFlushTime = Xbee.parseInt();
        while(!Xbee.available()>0)
        {
        }
        theFlag = true;
        runMode = FLUSH;
      }
      else if(key=='-')
      {
        Xbee.println("Manual Open");
        theHeroStepper.heroStep(10);
      }
      else if(key=='+')
      {
        Xbee.println("Manual Close");
        theHeroStepper.heroStep(-10);
      }
    }
  }  
}

void loop()
{
  if(runMode == RUN)
  {
    Serial.println("System is running");
    Xbee.println("System is running");
    int systemStartTime = millis();
    while((systemRunTime+systemStartTime)>(millis())&&runMode==RUN)
    {
      //salinitySensorLow.theTempSensor.addValue(salinitySensorLow.theTempSensor.getTemp());
      //salinitySensorHigh.theTempSensor.addValue(salinitySensorHigh.theTempSensor.getTemp());
      
      salinitySensorLow.addValue(salinitySensorLow.getSalinity());    
      salinitySensorHigh.addValue(salinitySensorHigh.getSalinity());
      
      float tempIntake = intakeFlowMeter.getFlowRate(intakeFlowCount);
      float tempBrine = brineFlowMeter.getFlowRate(brineFlowCount);
      float tempProduct = productFlowMeter.productGetFlowRate(tempIntake-tempBrine);
      
      intakeFlowMeter.addValue(tempIntake); 
      productFlowMeter.addValue(tempProduct);
      brineFlowMeter.addValue(tempBrine);
      
      pressureSensorLow.addValue(pressureSensorLow.getPressure());
      pressureSensorHigh.addValue(pressureSensorHigh.getPressure());
      
      acCurrentSensor.addValue(acCurrentSensor.getCurrent());
      dcCurrentSensor.addValue(dcCurrentSensor.getCurrent());
      
      bool theFlag = false;
      char key;
      if(mode==SERIALPRINT)
      {
        key = (char)Serial.read();
        if(key=='K')
        {
          Serial.println("Motors Killed");
          theActuators.killAllMotors();
          runMode=DONOTHING;
        }
        else if(key=='-')
        {
          Serial.println("Manual Open");
          theHeroStepper.heroStep(10);
          Serial.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='+')
        {
          Serial.println("Manual Close");
          theHeroStepper.heroStep(-10);
          Serial.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='/')
        {
          Serial.println("Manual Open Small");
          theHeroStepper.heroStep(1);
          Serial.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='*')
        {
          Serial.println("Manual Close Small");
          theHeroStepper.heroStep(-1);
          Serial.println(theHeroStepper.getCurrentStep());
        }
        }
      else if (mode==XBEEPRINT)
      {
        key = (char)Xbee.read();
        if(key=='K')
         {
          Xbee.println("Motors Killed");
          theActuators.killAllMotors();
          runMode=DONOTHING;
        }
        else if(key=='-')
        {
          Xbee.println("Manual Open");
          theHeroStepper.heroStep(10);
          Xbee.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='+')
        {
          Xbee.println("Manual Close");
          theHeroStepper.heroStep(-10);
          Xbee.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='/')
        {
          Xbee.println("Manual Open Small");
          theHeroStepper.heroStep(1);
          Xbee.println(theHeroStepper.getCurrentStep());
        }
        else if(key=='*')
        {
          Xbee.println("Manual Close Small");
          theHeroStepper.heroStep(-1);
          Xbee.println(theHeroStepper.getCurrentStep());
        } 
      }
      
      //controller
      float desiredPSI = 800.0;
      theHeroStepper.controller(desiredPSI, pressureSensorHigh.getPressure());
    
      if(millis()-SDTimer>SDUpdateRate)
      {
        theSDCard.write(millis(), salinitySensorLow.getFilteredValue(), salinitySensorHigh.getFilteredValue(), salinitySensorLow.theTempSensor.getFilteredValue(), salinitySensorHigh.theTempSensor.getFilteredValue(), 
          intakeFlowMeter.getFilteredValue(), productFlowMeter.getFilteredValue(), brineFlowMeter.getFilteredValue(), pressureSensorLow.getFilteredValue(), pressureSensorHigh.getFilteredValue(),
          acCurrentSensor.getFilteredValue(), dcCurrentSensor.getFilteredValue(), theHeroStepper.getError());
        SDTimer = millis();
      }
      /*while(acCurrentSensor.getFilteredValue()>3100.0&&acCurrentSensor.getFilteredValue()<3140.0)
      {
        Serial.println("System has been manually killed, pausing stepper motor");
        analogWrite(2,0);
      }*/
    }
    Serial.println("Run is complete");
    Xbee.println("Run is complete");
    runMode=FLUSH;
    /*if(intakeFlowMeter.getFilteredValue()==0.0)
    {
      Serial.println("ERROR!!! NO FLOW AT INTAKE!");
      runMode=DONOTHING;
    }*/
  }
  else if (runMode==FLUSH)
  {
    //flushing routine
    Serial.println("Solenoid Valves in Flush Mode");
    theActuators.killAllMotors();
    theActuators.flushMode();
    theHeroStepper.fullOpen();
    //disables stepper to conserve power
    analogWrite(2, 0);
    theActuators.enableMotor(lowPressureMotorPin);
    delay(2500);
    theActuators.enableMotor(highPressureMotorPin);
    float flushStartTime = millis();
    while(systemFlushTime+flushStartTime>millis())
    {
    }
    theActuators.allOff();
    Serial.println("System Flush Complete");
    Xbee.println("System Flush Complete");
    runMode = DONOTHING;
  }
  else if (runMode==DONOTHING)
  {
    while(1);
    //setup to be able to rerun system without reupload
  }
}//end of loop

void intakeISR()
{
  intakeFlowCount++;
}

void brineISR()
{
  brineFlowCount++;
}
