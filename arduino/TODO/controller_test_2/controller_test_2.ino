#define radio Serial  // change to Serial3 to route through XBee

#include <Stepper.h>  
#define STEPS 200
Stepper stepper(STEPS, 8, 9, 10, 11);  // stepper pins ? PWMH 10,9,8,7 or DIO 8,9,10,11?

#include <SPI.h>
#include <SD.h>

const int analogTest = A1;    // A1 on testing board not PCB

const int AC_CURRENT_SENSOR_OUT = A0;     // ... AC current draw
const int DC_CURRENT_SENSOR_OUT = A11;    // ... Dc current draw

int flushtime = 2;            // default cycle time in seconds
int runtime = 10;             // default cycle time in seconds
int OnOff = 0;

void setSD(void);
void writeSD(void);
void readSD();
     const int SD_CS = 32; // can add in card type, volume, root
void setRadio(void);    // sends and receives data and commands via RF with GUI. 
void RUN(void);
     char pump;
     char key;
     const int solenoid1 = 30;  // solenoid to open/close ....
     const int solenoid2 = 31;  // solenoid to open/close ....
     const int solenoid3 = 28;  // solenoid to open/close ....
     const int solenoid4 = 25;  // solenoid to open/close ....
void startMode(void);  // waits for the system to being fully operatable. times the action of the staring flow (tests only)
void setRun(void);       // 1 (pin 30) and 3 (pin 28) open, 2 (pin 31) and 4 (pin 25) closed
void setFlush(void);     // 1 (pin 30) and 3 (pin 28) closed, 2 (pin 31) and 4 (pin 25) open
void setFlow(void);     // set cases of each flow direction. 1-normal (setRun), 2-flush(setFlush), 3-allDischarge (Redundant?)
void pumpMode(void);    // 1-bothON, 2-bothOFF, 3-LowON_highOFF, 4-HighON_lowOFF (3 and 4 for testing only) Edited with T-Mo 3/1/17
     const int H_pump = 23;
     const int L_pump = 22;
void runMode(void);  // sets duration of run mode and runs for duration if not iterupted  
     bool enterControlLoopFlag = false;
void flushMode(void);   // sets duration of flush mode and runs for duration if not iterupted
void valveMode(void);   // allow independant control of each solenoid. prompt in format valveMode(valve, OpenClose);
     char valve;
     int OpenClose = 0;
void pressure_control(void);   // stepper control. consider pressure, flow, system mode
     int previous = 0; // previous step to pressure change

void runAnalog(void);    // testing only with potentiometer for representative data ("analogTest" variable)

void getFlow(void); float flow1; float flow2; float flow3;  // reads and allocates variable name for live flow data
     const int F_1_DATA = 51;     // flow sensor located...
     const int F_2_DATA = 53;     // flow sensor located...
     const int F_3_DATA = 49;     // flow sensor located...
     
void getPressure(void);  float pressure1 = A2; float pressure2;  // reads and allocates variable name for live pressure data
     const int P_1_DATA_IN = 2;     // (high/low?) pressure sensor located...
     const int P_2_DATA_IN = 7;     // (high/low?) pressure sensor located...
void getTemp(void);       float tempBox; float tempProduct;  // reads and allocates variable name for live temperature data
     const int T_1_DATA = 3;         // Temp sensor located... 
     const int TC_1_DATA_IN = 6;    // Temp and Conductivity sensor located...
     const int TC_2_DATA_IN = 1;    // ...added 3/1/17 second DIY salinity sensor
     void getSalinity(void); 
          float high_salinity; 
          float low_salinity;  // reads and allocates variable name for live temperature data
          const int C_1_DATA_IN = 5;     // ...conductivity data
          const int C_2_DATA_IN = 4;     // ...conductivity data
void storeData(void);  char live_data; char sd_data    // packet or string name needed to send multiple variabes at once per cycle for live stream
void getData(void);    // variable of all stored data to send over RF
void sendData(void);    // sends string of all data avaialble via RF

void shut_Down(void);    // sequence of pumps, valves, data logging, and powerdown of everything but the controller
void errorCheck(void);   // looks for errors by checking non zero returns from all sensors
     bool tempError_1 = false;
     bool tempError_2 = false;
     bool tempError_3 = false;
     bool pressureError = false;
     bool flowError_1 = false;
     bool flowError_2 = false;
     bool flowError_3 = false;
     bool salinityError_1 = false;
     bool salinityError_2 = false;
     bool AC_drawError = false;
     bool DC_drawError = false;
     bool testCheck = false;

/*
 * Notes
 * set interupt flag(s) for shut down sequence, bool enterControlLoopFlag=false; was created 3/1/17 by T Mo 
 */

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  //Serial.begin(9600);
  radio.begin(9600);
  stepper.setSpeed(40);  // 40 steps in one second or 1/5 hertz at 200 step revolutions (increase in testing for efficiency)
  
  pinMode(H_pump, OUTPUT);
  digitalWrite(H_pump, LOW);   // high pressure pump signal
  
  pinMode(L_pump, OUTPUT);
  digitalWrite(L_pump, LOW);   // low pressure pump signal
  
  pinMode(solenoid1, OUTPUT);
  digitalWrite(solenoid1,LOW);  // Green LED Solenoid 1
  
  pinMode(solenoid2, OUTPUT);
  digitalWrite(solenoid2,LOW);  // Orange LED Solenoid 2
  
  pinMode(solenoid3,OUTPUT);
  digitalWrite(solenoid3,LOW);   // Red LED Solenoid 3
  
  pinMode(solenoid4,OUTPUT);
  digitalWrite(solenoid4,LOW);  // Yellow LED Solenoid 4

 
  
  // Start up prompt
  while (! Serial); // Wait until Serial is ready - Leonardo
  
  setSD();
  
  radio.flush();
  radio.println();
  radio.println("Would you like to start the system (Y\N)"); radio.flush();
  
  while (radio.available() == 0) {}
  //when the system is fully ready and the user wants to run, flagSet is set to True
  //edit by T-Mo
  while(!enterControlLoopFlag)
  {
    RUN();
  }
}

// the loop function runs over and over again forever
void loop() 
{
  //will call error checks
  errorCheck();  
  //will stop if error thrown or some user input
  if (errorCheck() == true)
  {
    shut_Down();
  }
  //will stop and switch to flush mode after a set period of time
  pressure_control();
  //will write to SD
  writeSD();
}

void RUN()
{
  //runAnalog();
  
  
  if (radio.available() > 0)
  {
    
    key = (char)radio.read();
    char theBuffer;
    switch (key)
    {
      case 'N':
        pumpOFF();
        ShutDown();
        writeSD();
        break;
        
      case 'Y':
        startMode();
        radio.println(" ");
        radio.println("System Ready");
        delay(1000);
        radio.println("Enter R to change the runtime");
        radio.println("Enter F to change flushtime");
        radio.println("Enter W to start to operation cycle");
        break;
      
      case 'F':
      radio.println(" Enter flushtime whole number");
      while (radio.available() == 0){}
      if (radio.available()){
        setFlush();
      }
      break;

      case 'R':
      radio.println(" Enter runtime whole number");
      while (radio.available() == 0){}
      if (radio.available()){
        setRun();
      }
      break;

      case 'W':
      while (radio.available() == 0){
         
         runMode();  // Valves set to run feedwater
         pumpON();   // turns pumps on
                  
         float timeRun = 0;
         while ( timeRun < runtime)
         {
          if (timeRun < runtime)
          {
            timeRun++;
            timeRun = timeRun/100;
            delay(10) // 1/100 of seocnd
          }
         }
         pumpOFF();  //turns pumps off
         flushMode();
         pumpMode();
         float timeFlush = 0;
         while ( timeFlush < flushtime)
         {
          if (timeFlush < flushtime)
          {
            flushRun++;
            flushRun = flushRun/100; 
            delay(10) // 1/100 of seocnd
          }
         }     
      }      
      break;

      case 'P':
      radio.println("Rewrite this case for the testing the pumps");
      while (radio.available() == 0){};
      // pumpMode(pump, OnOff); 
      // input can be set to control a low pressure pump on only setting 
      // or high pressure pump setting on only to test ac and dc current draw
      break;

      case 'L':
      // open for use
      // if no use delete this case
      break;
    }
  }
  
}

void setSD()
{
  Serial.print("Initializing SD card...");
  pinMode(SD_CS, OUTPUT); //For the SD Card to work
  if (!SD.begin(SD_CS))
  {
    Serial.println("initialization failed!");
    return;
  }
  demoData = SD.open("demoData.csv", FILE_WRITE);
  if (demoData)
  {
    Serial.println("demoData file found..."); 
    String dataString = "Salinity_1, Salinity_2, Pressure_1, Pressure_2, Temp, Flow"; //edit this line as needed
    demoData = SD.open("demoData.csv", FILE_WRITE);
    demoData.println(dataString);
    demoData.close();
  }
}

// edit as needed
void writeSD()
{
  String dataString = String() + "," + String() + "," + String() + "," + String() + "," + String() + "," + String() + ", " + String()+", " + String();
    demoData = SD.open("demoData.csv", FILE_WRITE);
    demoData.println(dataString);
    demoData.close();
}

void readSD()
{
  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  File dataFile = SD.open("datalog.txt");

  // if the file is available, write to it:
  if (dataFile) {
    while (dataFile.available()) {
      radio.write(dataFile.read());   // declare variable here?
    }
    dataFile.close();
  }
  // if the file isn't open, pop up an error:
  else {
    radio.println("error opening datalog.txt");
}

void startMode()
{
  // initializes system electronics and checks for registering null data values
  /*
   * solenoid - not a check but a state of open
   * needle valve fully open
   * h/l pumps (test which one first and ac current sense with current draw tests)
   * flow, temp, pressure, salinity - check for non zero
   * check for steady state flow - 1/5 L/min?  
   */
  // stores ok value or nonregistering sensor with non ok value
  errorCheck();
  // allocates the value to send via RF
  writeSD();
  sendSD();
  
}

void runMode()
{
  pumpOFF();
  delay(50);
  digitalWrite(solenoid1, HIGH);
  digitalWrite(solenoid2, LOW);
  digitalWrite(solenoid3, HIGH);
  digitalWrite(solenoid4, LOW);
  delay(50);  
  pumpON();
  radio.println("Run mode in progress");
  
  enterControlLoopFlag = true;   // Edit by T-Mo to run pressure contro function with in main loop
  
}

void flushMode()
{
  pumpOFF()
  delay(50);
  digitalWrite(solenoid1, LOW);
  digitalWrite(solenoid2, HIGH);
  digitalWrite(solenoid3, LOW);
  digitalWrite(solenoid4, HIGH); 
  delay(50);
  pumpON();
  radio.println("Flush mode in progress");
}

void valveMode(char valve, int OpenClose)
{
  if (valve == '1' && OpenClose == 1)
  {
    digitalWrite(solenoid1, HIGH);
  }
  else if (valve == '1' && OpenClose == 0)
  {
      digitalWrite(solenoid1, LOW);
  } 
  else if (valve == '2' && OpenClose == 0)
  {
      digitalWrite(solenoid2, LOW);
  }
  else if (valve == '2' && OpenClose == 0)
  {
      digitalWrite(solenoid2, LOW);
  }
  else if (valve == '3' && OpenClose == 0)
  {
      digitalWrite(solenoid3, LOW);
  }
  else if (valve == '3' && OpenClose == 0)
  {
      digitalWrite(solenoid3, LOW);
  }
  else if (valve == '4' && OpenClose == 0)
  {
      digitalWrite(solenoid4, LOW);
  }
  else if (valve == '4' && OpenClose == 0)
  {
      digitalWrite(solenoid4, LOW);
  }
}

void shut_Down()
{
  // running tests
  // flow to discharge
  
  // pumps off
  pumpOFF();
  // data stored
  // power off electronics if temp in electronics box is the trigger
  // manually power off system controller otherwise after data is review and trouble shooting commenced
}

void errorCheck(void)
{
  // checks for value reutrned from all sensors, ac and dc current draw
  if (analoglogRead(T_1) == 0)
  {
    tempError_1 = true;
    testCheck = true;
  }
  if (analoglogRead(TC_1) == 0)
  {
    tempError_2 = true;
    testCheck = true;
  }
  if (analoglogRead(TC_2) == 0)
  {
    tempError_3 = true;
    testCheck = true;
  }
  if (analoglogRead(Temp_1) == 0)
  {
    pressureError = true;
    testCheck = true;
  }
  if (analoglogRead(F_1_DATA) == 0)
  {
    flowError_1 = true;
    testCheck = true;
  }
  if (analoglogRead(F_2_DATA) == 0)
  {
    flowError_2 = true;
    testCheck = true;
  }
  if (analoglogRead(F_3_DATA) == 0)
  {
    flowError_3 = true;
    testCheck = true;
  }
  if (analoglogRead(low_salinity) == 0)
  {
    salinityError_1 = true;
    testCheck = true;
  }
  if (analoglogRead(high_salinity) == 0)
  {
    salinityError_2 = true;
    testCheck = true;
  }
  if (analoglogRead(AC_CURRENT_SENSOR_OUT) == 0)
  {
    AC_drawError = true;
    testCheck = true;
  }
  if (analoglogRead(DC_CURRENT_SENSOR_OUT) == 0)
  {
    DC_drawError = true;  
    testCheck = true;
  }
  // can suggest solenoid malfunction with flow sensor
  // can suggest pump issue with ac and dc current draw
  // can suggest needle valve malfunction with flow and pressure
  // when serial.available == 0, the return is true

  // should shutdown be here?
  if (errorCheck() == true)
  {
    shut_Down();
    writeSD();
  }
}
void setFlush()
{
  flushtime=radio.parseInt();  // char to int 
  radio.print("Flush time set to: ");
  radio.print(flushtime);
  radio.println(" ");
}

void setRun()
{
  runtime=radio.parseInt();
  radio.print("Run time set to: ");
  radio.print(runtime);
  radio.println(" ");
}


void pumpMode(char pump, int OnOff)
{ 
  if (pump == 'H' && OnOff == 1)
  {
    digitalWrite(H_pump, HIGH);
    radio.println("High pressure pump on");
  }
  else if (pump == 'H' && OnOff == 0)
  {
      digitalWrite(H_pump, LOW);
      radio.println("High pressure pump off");
  } 
  else if (pump == 'L' && OnOff == 1)
  {
      digitalWrite(L_pump, HIGH);
      radio.println("Low pressure pump on");
  }
  else if (pump == 'L' && OnOff == 0)
  {
      digitalWrite(L_pump, LOW);
      radio.println("Low pressure pump off");
  }
  else
  { 
    radio.println("Pump error");
  }
}

void pressure_control()
{
  // open all the way by commanding maximum steps plus one in the open direct and set this position as reference
  // get the sensor values. Pressure and Flow
  // run loop checking for shut down sequence intermittantly

  //analogReadResolution(12);  
  int val = constrain(analogRead(A9), 0, 1026);
  int Value = map(val, 0, 1026, 0, 533);  
  int tol = 3;
  int stepValue =  Value - previous;
  
  //Serial.print(" Value ; "); Serial.print(val); Serial.println(" Step Value ; NO STEP");  
  Serial.print(val); Serial.print(stepValue); Serial.print(Value); Serial.println(previous);
  
  if (stepValue >= tol &&stepValue <= -tol)
  {
    Serial.print(" Value ; ");
    Serial.print(val);
    Serial.print(" Step Value ; ");
    Serial.print(stepValue);
    Serial.print(" Previous Value ; ");
    Serial.println(previous);
    // move a number of steps equal to the change in the
    // sensor reading
    stepper.step(stepValue);
    // remember the previous value of the sensor
    previous = Value;
    //delay(stepValue*1000/200/5);
    delay(50);
  }
  
}

void runAnalog()
{
  // **test function only**
  analogReadResolution(12);
  int analogRun = analogRead(analogTest);
  int sensorValue = analogRun;                             
  float voltage = sensorValue * (5.0 / 4096.0);
  radio.print(voltage); radio.print(" = voltage"); 
  radio.print(" : ");
  radio.print(sensorValue); radio.println(" = sensor value ");
}

void Flow_1(void) 
{
  count_1++;  // Every time this function is called, increment "count" by 1
} 

void Flow_2(void)
{
  count_2++;  // Every time this function is called, increment "count" by 1
}

void Flow_3(void)
{
  count_3++;  // Every time this function is called, increment "count" by 1
}


void Counter(void)
{
  /*
   * This funtion resets the count variable passed by the interrrupt ISR. The ISR is updated every time 
   * the pin attached reads low to high, RISING. The count value returns is the number of rotations
   * occuring during the length of delay set, 1/2 second. This opens and closes the interupt function 
   * for only that delay period. 
   */
  count_1 = 0;      // Reset the counter so we start counting from 0 again
  count_2 = 0;
  count_3 = 0;
  interrupts();   // Enables interrupts on the Arduino
  delay (500);    // Wait 1/2 second 
  noInterrupts(); // Disable the interrupts on the Arduino
}

void getFlow()
{
  // read flow sensors
  // allocate values to variables
  // Counter() returns the count of rotations in half a second. 
  // The flowRate equation converts the rotations per half second into mL per half second   ????
  
  Counter();                          // resets counter for a reading 
  flowRate_1 = (count_1 * 2.25);      //Take counted pulses in the last second and multiply by 2.25mL 
  flowRate_2 = (count_2 * 2.25);
  flowRate_3 = (count_3 * 2.25);   
  flowRate_1 = flowRate_1 * 60;       //Convert seconds to minutes, giving you mL / Minute
  flowRate_2 = flowRate_2 * 60;
  flowRate_3 = flowRate_3 * 60;   
  flowRate_1 = flowRate_1 / 1000;     //Convert mL to Liters, giving you Liters / Minute
  flowRate_2 = flowRate_2 / 1000; 
  flowRate_3 = flowRate_3 / 1000;        
  digitalRead(flowPin_1);  digitalRead(flowPin_2);  digitalRead(flowPin_3);
  
  // Print the variable flowRate to Serial for testing and when using putty
  // when running with gui, write data to SD and transmitted string
  Serial.print("Flow Rate 1 = "); Serial.println(flowRate_1);    
  Serial.print("Flow Rate 2 = "); Serial.println(flowRate_2); 
  Serial.print("Flow Rate 3 = "); Serial.println(flowRate_3);
  
}

void getPressure()
{
  // read pressure sensors
  HIGH_pressure = analogRead(high_pressure);
  LOW_pressure = analogRead(low)pressure);
  // convert DAQ to PSI
  
  // allocate values to variables
}

void getTemp()
{
  // read temp sensors
  // convert DAQ to Fahrenheit
  // allocate values to variables
}

void getSalinity()
{
  // read conductivity sensor data
  // read temperature sensor data
  // implement proper DAQ conversions
  // calculate salinity values
  // allocate values to variables
}

void getData()
{
  // organize all data values into one string
  // find a particular value if called, from sd
  
}

void storeData()
{
  // store data strings on sd
  // called in main loop to store data values during the loop 
  
}

void sendData()
{
  // transmit data string via RF with GUI interface
  // read incoming transmitions for commands new variables values
  // new run time or flush time, shut down, salinity, or pressure
}

