# README #

This README would normally document whatever steps are necessary to get your application up and running.

### OME Team HERO Senior Design 2016-2017 repository ###
* This project is currently maintained by Travis Moscicki travismoscicki@gmail.com
* The software team for the project consists of John Denton and John Sebelle
* Team HERO consists of John Denton, Steven Jarrard, Travis Moscicki, Travis Roy, John Sebelle, Alex Smith, and Christina Tsai

### Package Information ###
* Arduino Version - 1.8.2
* Due Library Version - 1.6.11
* Developed in Ubuntu 16.04
* The OneWire library is required.  The latest version is available at https://github.com/PaulStoffregen/OneWire.
* HERO Software Version - 1.9

### The sensors and actuators leveraged in this protect include ###

* Temperature Sensor
* High Salinity Sensor
* Low Salinity Sensor
* Flow Meter
* Stepper Motor
* High Pressure Sensor
* Low Pressure Sensor
* Solenoid Valves
* Current Sensors
* High Pressure Motor
* Low Pressure Motor

### Arduino ###

* To use this package, place the contents of the `libraries` folder wherever you have the Arduino IDE set to look for libraries.
* The file `loop.ino` is the main script for the software package.  The enum type `THEMODE` can be set to `DEEPDEBUG`, `DEBUG`, or `RUN`.
* `DEEPDEBUG` will serial print all variables used during development
* `DEBUG` will serial print just the main system variables 
* `RUN` will print the pertinent system variables to the XBee RF modem

### IMPORTANT ###
* To overcome current issues with the selected stepper motor controller and power supply, the default PWM frequency of the Arduino DUE must be changed.
* In Windows 10 (and perhaps other variants), this can be done by
* In Ubuntu 16.04 (and perhaps other variants), this can be done by:
* navigating to '~/.arduino15/packages/arduino/hardware/sam/1.6.11/variants/arduino_due_x'
* opening file 'variant.h'
* changing line 226 from '#define PWM_FREQUENCY		1000' to '#define PWM_FREQUENCY		5000'

### TODO ###
* Add comments
* Update salinity - 2.0
* Update conversion from psi to step
* Update conversion from byte to psi
* Update conversion from byte to current
* Update conversion from byte to temperature - 3.0
* Update debug messages
* Update XBee print - 4.0
* Update GUI - 5.0
