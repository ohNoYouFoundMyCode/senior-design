#-------------------------------------------------
#
# Project created by QtCreator 2017-04-16T19:47:33
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = theHero
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp\
        mainwindow.cpp \
    ../../../../Downloads/qwt-6.1.3/src/qwt_dial.cpp \
    ../../../../Downloads/qwt-6.1.3/src/qwt_dial_needle.cpp \
    ../../../../Downloads/qwt-6.1.3/src/qwt_compass.cpp

HEADERS  += mainwindow.h \
    ../../../../Downloads/qwt-6.1.3/src/qwt.h \
    ../../../../Downloads/qwt-6.1.3/src/qwt_dial.h \
    ../../../../Downloads/qwt-6.1.3/src/qwt_dial_needle.h \
    ../../../../Downloads/qwt-6.1.3/src/qwt_compass.h

FORMS    += mainwindow.ui

unix:!macx: LIBS += -L$$PWD/../../../../../../usr/local/qwt-6.1.3/lib/ -lqwt

INCLUDEPATH += $$PWD/../../../../../../usr/local/qwt-6.1.3/include
DEPENDPATH += $$PWD/../../../../../../usr/local/qwt-6.1.3/include

CONFIG+=qwt
