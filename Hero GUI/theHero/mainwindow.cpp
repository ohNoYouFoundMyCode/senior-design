#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSerialPort>
#include <QSerialPortInfo>
#include <qdebug.h>
#include <qpainter.h>
#include <qwt.h>
#include <qwt_dial.h>
#include <qwt_dial_needle.h>
#include <qwt_compass.h>
#include <QPainter>
#include <QTimer>

QSerialPort *theSerialPort;
bool runModeFlag=false;
bool flushModeFlag=false;
bool pressureSetFlag=false;
bool firstFlag=true;
bool secondFlag=true;
QTimer *theTimer;
float runTimer = 0;
bool startRunTimer = false;
float flushTimer = 0;
bool startFlushTimer = false;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->textEdit_2->setText(QString::number(0));
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
        ui->comboBox_2->addItem(info.portName());
    theSerialPort = new QSerialPort(this);
    connect(theSerialPort,SIGNAL(readyRead()), this, SLOT(serialReceived()));

    serialBuffer = "";
    ui->theDial->setScale(0,1000);
    ui->theDial->setScaleArc(30,330);
    theTimer = new QTimer(this);
    connect(theTimer,SIGNAL(timeout()), this, SLOT(update()));
    theTimer->start(1000);
}

MainWindow::~MainWindow()
{

    theSerialPort->close();
    delete ui;
}

void MainWindow::on_pushButton_10_clicked()
{
    ui->theDial->setValue(ui->textEdit_2->toPlainText().toInt()-1);
    ui->textEdit_2->setText(QString::number(ui->theDial->value()));
}

void MainWindow::on_pushButton_11_clicked()
{
    ui->theDial->setValue(ui->textEdit_2->toPlainText().toInt()+1);
    ui->textEdit_2->setText(QString::number(ui->theDial->value()));
}

void MainWindow::on_pushButton_12_clicked()
{

        theSerialPort->setPortName(ui->comboBox_2->currentText());

        theSerialPort->setBaudRate(QSerialPort::Baud9600);

        theSerialPort->setDataBits(QSerialPort::Data8);

        theSerialPort->setParity(QSerialPort::NoParity);

        theSerialPort->setStopBits(QSerialPort::OneStop);

        theSerialPort->setFlowControl(QSerialPort::NoFlowControl);

    if(
        theSerialPort->open(QIODevice::ReadWrite))
    {
        ui->textBrowser_4->setText("Connected");
    }
    else
    {
        ui->textBrowser_4->setText("Problem opening serial port");
    }

}

void MainWindow::on_pushButton_13_clicked()
{
    firstFlag=true;
    theSerialPort->close();
    ui->textBrowser_4->setText("Disconncted");
}

void MainWindow::on_pushButton_15_clicked()
{
    runModeFlag=true;
}

void MainWindow::on_pushButton_14_clicked()
{
    flushModeFlag=true;
}


void MainWindow::on_pushButton_7_clicked()
{
    pressureSetFlag=true;
}

void MainWindow::on_pushButton_6_clicked()
{
    if(
        theSerialPort->isOpen())
    {
        if(runModeFlag&&flushModeFlag&&pressureSetFlag)
        {
            ui->textBrowser_4->setText("System Will Start");
            theSerialPort->write("R");
            //writes run time
            int tempInt = ui->plainTextEdit_4->toPlainText().toInt()*1000;
            QString temp = QString::number(tempInt);
            theSerialPort->write(temp.toUtf8());
            startRunTimer = true;
            theSerialPort->write("\n");
            //writes flush time
            int tempInt2 = ui->plainTextEdit_3->toPlainText().toInt()*1000;
            QString temp2 = QString::number(tempInt2);
            theSerialPort->write(temp2.toUtf8());
            theSerialPort->write("\n");
            //writes flush time
            QString temp3 = ui->textEdit_2->toPlainText();
            theSerialPort->write(temp3.toUtf8());
            theSerialPort->write("\n");
        }
        else if(!runModeFlag&&flushModeFlag&&pressureSetFlag)
        {
            ui->textBrowser_4->setText("Please set runnning time");
        }
        else if(!flushModeFlag&&runModeFlag&&pressureSetFlag)
        {
            ui->textBrowser_4->setText("Please set flushing time");
        }
        else if(!pressureSetFlag&&runModeFlag&&flushModeFlag)
        {
            ui->textBrowser_4->setText("Please set operating pressure");
        }
        else if(!runModeFlag&&!flushModeFlag&&pressureSetFlag)
        {
            ui->textBrowser_4->setText("Please set running time and flushing time");
        }
        else if(!runModeFlag&&!pressureSetFlag&&flushModeFlag)
        {
            ui->textBrowser_4->setText("Please set running time and operating pressure");
        }
        else if(runModeFlag&&!pressureSetFlag&&!flushModeFlag)
        {
            ui->textBrowser_4->setText("Please set flushing time and operating pressure");
        }
        else
        {
            ui->textBrowser_4->setText("Please set running time, flushing time, and operating pressure");
        }
    }
    else
    {
        ui->textBrowser_4->setText("Please connect serial port before continuing");
    }
}

void MainWindow::serialReceived()
{
    serialData = theSerialPort->readAll();
    serialBuffer += QString::fromStdString(serialData.toStdString());
    QStringList startString = serialBuffer.split(QRegExp("\\n+"));
    qDebug()<<serialBuffer;
    if(firstFlag&&serialBuffer.size()==287)
    {
        QStringList firstString = serialBuffer.split('$');
        ui->textBrowser_3->setText(firstString[1]);
        firstFlag=false;
        serialBuffer.clear();
        startString.clear();
    }
    if(startString.length()==10)
    {
        QStringList lowSalString = startString[0].split(":");
        QString temp = lowSalString[1].split(";")[0];
        qDebug()<<"lowSal";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_10->display(temp.toFloat());

        QStringList highSalString = startString[1].split(":");
        temp = highSalString[1].split(";")[0];
        qDebug()<<"highSal";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_9->display(temp.toFloat());

        QStringList intakeFlowString = startString[2].split(":");
        temp = intakeFlowString[1].split(";")[0];
        qDebug()<<"intakeFlow";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_12->display(temp.toFloat());

        QStringList brineFlowString = startString[3].split(":");
        temp = brineFlowString[1].split(";")[0];
        qDebug()<<"brineFlow";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_13->display(temp.toFloat());

        QStringList productFlowString = startString[4].split(":");
        temp = productFlowString[1].split(";")[0];
        qDebug()<<"productFlow";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_14->display(temp.toFloat());

        QStringList lowPressureString = startString[5].split(":");
        //temp =
        //ui->->setText(startString[5]);

        QStringList highPressureString = startString[6].split(":");
        temp = highPressureString[1].split(";")[0];
        qDebug()<<"highPressure";
        //qDebug()<<temp.toFloat();
        ui->dial->setValue(temp.toFloat());

        QStringList acCurrentString = startString[7].split(":");
        temp = acCurrentString[1].split(";")[0];
        qDebug()<<"acCurrent";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_15->display(temp.toFloat());

        QStringList dcCurrentString = startString[8].split(":");
        temp = dcCurrentString[1].split(";")[0];
        qDebug()<<"dcCurrent";
        //qDebug()<<temp.toFloat();
        ui->lcdNumber_15->display(temp.toFloat());

        serialBuffer.clear();
    }
}

void MainWindow::on_pushButton_clicked()
{
    serialBuffer.clear();
    ui->textBrowser_3->setText(serialBuffer);
}

void MainWindow::update()
{
    if(runTimer>=ui->plainTextEdit_4->toPlainText().toInt()&&runTimer!=0)
    {
        startFlushTimer=true;
    }
    if(runTimer<ui->plainTextEdit_4->toPlainText().toInt()&&startRunTimer)
    {
        runTimer++;
        float tempFloat = runTimer/ui->plainTextEdit_4->toPlainText().toFloat();
        //qDebug()<<qRound(tempFloat*100);
        ui->progressBar_4->setValue(qRound(tempFloat*100));
    }
    else if(flushTimer<ui->plainTextEdit_3->toPlainText().toInt()&&startFlushTimer)
    {
        flushTimer++;
        float tempFloat = flushTimer/ui->plainTextEdit_3->toPlainText().toFloat();
        //qDebug()<<qRound(tempFloat*100);
        ui->progressBar_3->setValue(qRound(tempFloat*100));
    }
}
