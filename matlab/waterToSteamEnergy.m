clc
clear all
close all
%% Amount of energy needed to turn water at different degrees into steam
%enthalpy of vaporization in kJ/kg from 20 degrees C to 60 degrees C in
%steps of 5 degrees
hfg = [2454 2442 2431 2419 2407 2395 2383 2371 2359]; 
%1 liter of water = 1 kg
mdot = linspace(.5, 4, 7);%kg
qDotNeeded = zeros(length(mdot), length(hfg));
for i=1:length(mdot);
    for j=1:length(hfg)
        %this is the watts required to convert the water to steam over a
        %single day
        qDotNeeded(i,j) = 1000*mdot(i)*hfg(j)/(60*60*24);
    end
end

% The purpose of this file is to (a) determine the heat transfer (in watts) out of a pipe
% due to an external film of water (b) the required input flow rate as a
% function of outlet flow rate
surfaceTemp = 160; %in degrees C
mediumTemp = linspace(20, 60, 9); %in degrees C
tempFilm = (surfaceTemp+mediumTemp)/2;
%% Properties of water at different temps (From table A-2)
temp = [20 30 40 50 60];
tempFull = [20 25 30 35 40 45 50 55 60];

k = [0.5984 0.6154 0.6305 0.6435 0.6543]; %W/m*DegreesCelsius
kFull = interp1(temp,k,tempFull,'spline');

kinematicViscosity = [1.004*10^(-6) 0.801*10^(-6) 0.658*10^(-6) 0.553*10^(-6) 0.474*10^(-6)];%m^2/s
kinematicViscosityFull = interp1(temp,kinematicViscosity,tempFull,'spline');

Pr = [1.004*10^(-6) 0.801*10^(-6) 0.658*10^(-6) 0.553*10^(-6) 0.474*10^(-6)];%non-dimensional
PrFull = interp1(temp,Pr,tempFull,'spline');

%figure
%plot(tempFull,kFull,'o');
%title('Spline Interpolation');
%figure
%plot(tempFull,kinematicViscosityFull,'o');
%title('Spline Interpolation');
%figure
%plot(tempFull,PrFull,'o');
%title('Spline Interpolation');
%% Obtaining h values
velocityConstant = 0.1;%m/s in steps of .5
diameterConstant = 0.0254;% in meters
Re = velocityConstant*diameterConstant./kinematicViscosityFull;%with constant velocity and variable diameter

Nu = zeros(length(Re));
for i=1:length(Re)
    Nu(i)=0.3+((0.62*Re(i)^(1/2)*PrFull(i)^(1/3))/((1+(0.4/PrFull(i))^(2/3))^1/4))*((1+(Re(i)/282000)^(5/8))^(4/5));
end

h = zeros(length(Nu));
for i=1:length(Nu)
    h(i)=kFull(i)*diameterConstant/Nu(i);
end
%% Obtaining Energy Transfer Rate
surfaceArea=3.1415.*diameterConstant;%per meter of pipe
qDotGenerated = zeros(length(Nu),1);
for i=1:length(h)
    qDotGenerated(i) = h(i)*surfaceArea*(surfaceTemp-mediumTemp(i));
end
%% The Plot
figure
hold on
for i=1:length(qDotGenerated)
    plot(linspace(0, 300, 300), linspace(0,300, 300)*qDotGenerated(i))
    for j=1:7
        plot(qDotNeeded(j,i)/qDotGenerated(i), qDotNeeded(j,i)/qDotGenerated(i)*qDotGenerated(i),'o')
    end
end

title({'Power Transfer as a function of temperature vs length of tube';'Circles depict where flow rates from .5 to 4 m/s in steps of .5 are obtained'})
%legend('qDot(20)', 'qDot(25)', 'qDot(30)', 'qDot(35)','qDot(40)', 'qDot(45)','qDot(50)', 'qDot(55)', 'qDot(60)')
xlabel('Length of the pipe in meters')
ylabel('Power Transfer in watts')