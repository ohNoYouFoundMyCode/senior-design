close all     %close previously opened figures

%The Reverse Osmosis Mathmatical Model%
%The goeverning equations are dervied from the collerelations 
%of flow rate as function of filter production rating Q(cm^3/s) for brine
%and product water. The salinty concentration are assumed from an online
%source and can be later given actual values for testing environment.

C_p = 0.5;  %product salinty parts per thousand is a constant
C_f = 3.5;  %feedwater salinty parts per thousand is a constant

Q_pmax = 16.68*4;          %(cm^3/s) is the maximum desired product flow rate
%Q_f = 0.1*Q_f + 0.9*Q_f;    %Feedwater flow rate correlation
%Q_p = 0.1*Q_f;              %Product water flow rate proportion of feed
%Q_b = 0.9*Q_f;              %Brine water flow rate proportion of feed
                               
  
%the folling claculation plots product and brine flow rates with respect to
%feedwater flow rate
Q_p = linspace(0, Q_pmax, 100); 
Q_f = Q_p/0.1;
Q_b = 0.9*Q_f;
subplot(2,1,1), plot(Q_p, Q_f)  
title('Feedwater, Brine, Product Volumetric Flow Rates')
xlabel('Product Water Volumetric Flow Rates (cm^3/s)')
grid on
subplot(2,1,2), plot(Q_b, Q_f)  
xlabel('Brine Water Volumetric Flow Rate')
ylabel('Feedwater Volumetric Flow Rate')
grid on

%the following shows product and brine flow rates simutaneously with
%respect to feedwater flow rate
figure 
plot(Q_p, Q_f, 'r') 
hold on 
plot(Q_b, Q_f, 'b')
title('Brine and Product Water with Respect to Feedwater')
xlabel('Red-Product, Blue-Brine Volumetric Flow Rate (cm^3/s)')
ylabel('Feedwater Volumetric Flow Rate')
grid on

%given the salinty of existing salt water and desired product water, the
%following calculates an estimated brine water salinty concentration
%Q_f*C_f = Q_b*C_b + Q_p*C_p  %equation found from an online research source
Q_p = 0.1*Q_f;   %proportion of feedwater based on filter ratings (Variable) 
Q_b = 0.9*Q_f;   %proportion of Brine also based on filter ratings (Variables)
C_b = (Q_f*C_f - Q_p*C_p)/(Q_b); %Brine Concentration

%plot of brine concentration is a constant with respect to any flow rate
figure
plot(C_b, Q_p,'o r')
title('Brine Water Salinity with respect to Product Water Volumetric Flow Water')
xlabel('Brine Salinity (%)')
ylabel('Product Volumetric Flow rate (cm^3/s)')
grid on

%Q_1=Q_2=Q_3=Q_f=Q_p+Q_b      % system design balance of working volumes in
                              %pretreatment water tanks and filter feed rate.  
                              %this maintains a constant supply in the 
                              %treatment tanks
% Q_f = Q_p + Q_b is desired


%PUMP POWER REQUIREMENTS
r_1 = .02;                         %assuming 4 centimeter diameter inlet pipe
V_1 = (Q_f*(100^-3)) / (pi*r_1^2); %flow velocity of feed water pumped into pretreatment
v_seawater = 1.83*10^-6;           %kinematic viscosity of seawater (m^2/s)
Re_1 = V_1*(2*r_1)/v_seawater;     %Reynolds number of incoming feedwater
f_1 = 0.1;  %friction assumed smooth but otherwise function of the reynolds number
k_e = 0.5;  %loss coefficient for pipe entrance
k_b = .35;  %loss coefficient for pipe bend
g = 9.8;    %acceleration due to gravity, constant
L_1 = 2.5;  %assumed inlet pipe length (m)

%the following formula calculates head losses in the pipe
h_L = (V_1.^2/(2*g))*(f_1*(L_1 /(2*r_1)) + k_e + k_b);
%the head losses are then used to calculate the total pump head added
p_atm = 101325/1000; %atmosphereic pressure in kilopascals
p_1 = p_atm;    %pressure at sea level
p_2 = p_atm;    %pressure at pretreatment level
gamma = 10.03*1000;    %specific weight of seawater (N/m^3) 
z_1 = 0;    %sealevel elevation (m)
z_2 = 2;    %ppretreatment tank elevation (m)
%pump head
h_P = (((p_2-p_1)/gamma)^2 + (z_2 - z_1)) + h_L;
%required pump horsepower as function of feedwater flow rate
P_f = gamma.*(Q_f.*(100^-3).*h_P);
P_F_HP = P_f/746;
%plot of pump power required with respect to feedwater flow rate
figure
subplot(2,1,1), plot(P_F_HP, Q_f)
xlabel('Horsepower Required')
grid on
%same plot but 
title('Inlet Power with Respect to Vol. Flow Rate                       .')
subplot(2,1,2), plot(P_f, Q_f)
xlabel('Watts Required')
ylabel('Feedwater Volumetric Flow Rate (cm^3/s)')
grid on

%PUMP POWER REQUIRED FOR 1000 PSI REQUIREMENT OF RO FILTER
%1 Psi = 6.89475729 Kilopascals 
p_filter = linspace(0, 1000*6.89475729, 100);
p_filter_psi = linspace(0, 1000, 100);
r_2 = .01;                   %assuming 2 centimeter diameter filter pipe
V_2 = (Q_f*(100^-3)) / (pi*r_2^2);
L_2 = 1;

%the following formula calculates head losses in the pipe
h_L2 = (V_2.^2/(2*g))*(f_1*(L_2 /(2*r_2)) + k_e );
%z_2 = 2;    %sealevel elevation (m) (already initialized)
z_3 = 2;    %pretreatment tank elevation (m)

%pump head
h_P2 = (((p_filter-p_2)/gamma).^2 + (z_3 - z_2)) + h_L2;
%power required to increase pressure required by RO filter
P_f2 = gamma.*(Q_f.*(100^-3).*h_P2); %Horsepower
P_F2_HP = P_f2/746;                  %Watts
%plot of pump horsepower required with respect to feedwater flow rate
figure
subplot(2,1,1), plot(P_F2_HP, Q_f)
xlabel('Horsepower Required')
grid on
%same plot with watts 
title('Filter Pressure Power with Respect to Vol. Flow Rate                       .')
subplot(2,1,2), plot(P_f2, Q_f)
xlabel('Watts Required')
ylabel('Feedwater Volumetric Flow Rate (cm^3/s)')
grid on
%plot of pressure in pascals to horsepower and watts
figure
subplot(2,1,1), plot(P_f2, p_filter)
title('Pressure to Power Relationship')
xlabel('Power in Horsepower')
grid on
subplot(2,1,2), plot(P_F2_HP, p_filter)
xlabel('Power in Watts')
ylabel('Pressure (Pa)')
grid on
%plot of pressure in psi to horsepower and watts
figure
subplot(2,1,1), plot(P_f2, p_filter_psi)
title('Pressure to Power Relationship')
xlabel('Power in Horsepower')
grid on
subplot(2,1,2), plot(P_F2_HP, p_filter_psi)
xlabel('Power in Watts')
ylabel('Pressure (Psi)')
grid on